SHELL := /bin/bash
#-----------------------------------------------
#    __  __       _         __ _ _
#   |  \/  | __ _| | _____ / _(_) | ___
#   | |\/| |/ _  | |/ / _ \ |_| | |/ _ \
#   | |  | | (_| |   <  __/  _| | |  __/
#   |_|  |_|\__,_|_|\_\___|_| |_|_|\___|
#
#-----------------------------------------------
#              LabViewPlotter
#-----------------------------------------------
#	Author:      Rossana
#	Email:       ingrossrv@gmail.com
#	Github:      https://bitbucket.org/RossRV/
#
#	Description: This Makefile may be useful to
#	             update theGUI App that plot
#	             *.lvm files produced by LabView
#-----------------------------------------------

# Dirs & Files
# -------------
QT_DIR = Qt
PYTHON_DIR = python
QT_UI = mainwindow.ui
PY_UI = plotterQt.py
RES_QT = resources.qrc
RES_PY = resources_rc.py
GUI = LabViewPlotterGUI.py

# Commands
# -------------
PYUIC = pyuic4
PYRCC = pyrcc4
PYTHON = python2.7

#-----------------------------------------------

.PHONY: run clean translate

default: run

run:
	@echo -e "\nRunning...\n\t $(REDC)$(PYTHON_DIR)/$(GUI)\n$(ENDC)"
	@cd $(PYTHON_DIR) && $(PYTHON) $(GUI)

translate:
	$(PYUIC) $(QT_DIR)/$(QT_UI) -o $(PYTHON_DIR)/$(PY_UI)
	$(PYRCC) $(QT_DIR)/$(RES_QT) -o $(PYTHON_DIR)/$(RES_PY)

clean:
	@echo -e "$(REDC)\nNothing to clean ...$(ENDC)"

# -----------------------------
# Some styles and colors to be
# used in Terminal outputs
# -----------------------------
REDC = \033[31m
BOLD = \033[1m
GREENC = \033[32m
UNDERLINE = \033[4m
ENDC = \033[0m
# -----------------------------

# --------------------------------------------------------------
help:
	@echo -e "\n------------------------------------------------------"
	@echo -e "\t\t$(UNDERLINE)$(REDC)< LabViewPlotter >$(ENDC)"
	@echo -e "\t\t$(GREENC)   Makefile Menu$(ENDC)"
	@echo -e "------------------------------------------------------"
	@echo -e "Please use 'make <target>' where target is one of:\n"
	@echo -e "$(REDC)default$(ENDC)   $(GREENC)run$(ENDC)\n"
	@echo -e "$(REDC)run$(ENDC)       to run $(PYTHON_DIR)/$(GUI)\n"
	@echo -e "$(REDC)translate$(ENDC) to translate Qt >> PyQt4\n"
	@echo -e "$(REDC)clean$(ENDC)     to clean STUFF.\n"
# --------------------------------------------------------------
