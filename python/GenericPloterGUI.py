"""Simple GUI App for Plot LVM LabView Native Data Files."""
from __future__ import division
from __future__ import print_function

from PyQt4.QtGui import QMainWindow, QApplication, QFileDialog, QIcon
from PyQt4 import QtCore
from plotterQt import Ui_MainWindow
from style_setter import set_styles
import sys
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
import logging as log

# ------------------------------------------------------
_author = "RossRV"
_mail = "rossana@iim.unam.mx"
_dataDir = "./data/"
_plotsDir = "./plots/"
_plotFormat = "pdf"
# _lvm_format = {"sep": "\t", "column_names": ['empty', 't', 'I']}
sns.set_context('paper', font_scale=1.3, )  # notebook, poster, talk, paper
# sns.set_style("ticks")  # darkgrid, whitegrid, dark, white, ticks
sns.set_style('ticks', {'font.size': 20, 'xtick.direction': u'in', 'ytick.direction': u'in', })
# sns.set_palette("bright")  # deep, muted, pastel, bright, dark, and colorblind
sns.set_color_codes("bright")

_theme_style = "./css/Materials_theme/"  # Comment this to use the original style defined by Qt Creator

_log_file = "./logs/labviewGUI.log"
_DEBUG = True
# ------------------------------------------------------

if _DEBUG:
    log.basicConfig(filename=_log_file,
                    level=log.DEBUG,
                    format='%(asctime)s %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')


class Window(QMainWindow, Ui_MainWindow):
    """Main Window class."""
    # global _plotFormat

    def __init__(self):
        """Initialize the Gui App."""
        # super(Window, self).__init__(None, QtCore.Qt.WindowStaysOnTopHint)
        super(Window, self).__init__()
        # Set up the user interface from Designer.
        self.setupUi(self)
        self.filename_data = None
        self.setWindowIcon(QIcon('./icons/LabView_64px.png'))
        self.plotSavedLabel.setText("Nothing Saved")
        self.load_style()
        self.authorLabel.setText("{}: {}".format(_author, _mail))
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        self.figure_canvas = FigureCanvas(self.fig)
        self.canvas = self.figure_canvas
        self.toolbar = None
        self.plots_dir = _plotsDir
        self.has_plot = False
        self.plotFormat = _plotFormat
        self.data = None
        self.plotWindow.resize(self.plotWindow.width(), self.plotWindow.height()-30)

    def run(self):
        """Display the Gui."""
        self.plotBtn.clicked.connect(self.plot)
        # self.clearPlotBtn.clicked.connect(self.clear_plots)
        self.chargeBtn.clicked.connect(self.charge)
        self.selectFileBtn.clicked.connect(self.load_file)
        # self.quitBtn.clicked.connect(self.quit)
        # mainMenu = self.menuBar
        # fileMenu = self.menuFile.
        self.actionOpen.triggered.connect(self.load_file)
        self.actionQuit.triggered.connect(self.quit)
        self.actionFull_Screen.triggered.connect(self.showFullScreen)
        self.actionMaximize.triggered.connect(self.showMaximized)
        self.chooseTheme_1.triggered.connect(lambda: self.load_style("Qt_theme"))
        self.chooseTheme_2.triggered.connect(lambda: self.load_style("Materials_theme"))
        self.customPlotsDir.triggered.connect(lambda: self.set_plots_dir("custom"))
        self.changePlotsDir.triggered.connect(self.set_plots_dir)
        # PlotFormats
        self.formatComboBox.addItems(["pdf", "jpg", "png", "eps"])
        self.show()
        # self.showMaximized()

    def set_plots_dir(self, custom=None):
        if custom:
            self.plots_dir = _plotsDir
            return
        self.plots_dir = str(QFileDialog.getExistingDirectory(self, 'Save Plots At ...', ".", QFileDialog.ShowDirsOnly))
        print(self.plots_dir)

    def load_style(self, theme="Materials_theme"):
        css = None
        if theme == "Qt_theme":  # Default
            css = "./css/Qt_theme/"
        elif theme == "Materials_theme":
            css = "./css/Materials_theme/"
        else:
            css = "./css/Materials_theme/"

        set_styles(self, css)


    def quit(self):
        """Kill ALL."""
        QtCore.QCoreApplication.instance().quit()
        if _DEBUG:
            log.debug("App Quit")

    def plot(self):
        """Get and Display the Plot."""
        if self.has_plot:
            self.clear_plots()
            return
        if self.data is None:
            print("Warning: Not plot produced because data is not yet loaded")
            if _DEBUG:
                log.warn("Not plot produced because data is not yet loaded")
            with open(_theme_style+"fileLoadedLabel.css", "r") as f_css:
                self.fileLoadedLabel.setStyleSheet(f_css.read())
            return

        save_plot = False
        plot_name = None
        if self.savePlotCheckBox.isChecked():
            save_plot = True
            self.plotFormat = str(self.formatComboBox.currentText())
            filename, file_extension = os.path.splitext(os.path.basename(self.filename_data))
            plot_name = os.path.join(self.plots_dir, filename + "." + self.plotFormat)
            self.plotSavedLabel.setText(plot_name)

        X = self.data[str(self.xComboBox.currentText())]
        Y = self.data[str(self.yComboBox.currentText())]

        get_plot(ax=self.ax, X=X, Y=Y,
                 save=save_plot, plot_name=plot_name, plot_format=_plotFormat)
        self.ax.set_xlabel("column: " + str(self.xComboBox.currentText()))
        self.ax.set_ylabel("column: " + str(self.yComboBox.currentText()))

        self.ax.set_title(os.path.basename(self.filename_data))
        self.mplVerticalLayout.addWidget(self.canvas)
        self.canvas.draw()
        self.toolbar = NavigationToolbar(self.canvas, self.plotWindow, coordinates=True)
        self.mplVerticalLayout.addWidget(self.toolbar)
        self.has_plot = True
        self.plotBtn.setText("Clear")

    def clear_plots(self):
        """Clean the Display region."""
        if self.has_plot:
            # self.mplVerticalLayout.removeWidget(self.canvas)
            self.ax.clear()
            # self.canvas.close()
            self.mplVerticalLayout.removeWidget(self.toolbar)
            self.toolbar.close()
            self.has_plot = False
            self.plotSavedLabel.setText("Nothing Saved")
            self.plotBtn.setText("Plot")
        else:
            pass

    def charge(self):
        """Obtain the deposited charge."""
        if self.data is not None:
            c = "{:.3f} C".format(get_charge(self.data))
            self.chargeLabel.setText(c)
        else:
            print("Warning: Not Charge calculated because data is not yet loaded")
            if _DEBUG:
                log.warn("Not Charge calculated because data is not yet loaded")
            with open(_theme_style+"fileLoadedLabel.css", "r") as f_css:
                self.fileLoadedLabel.setStyleSheet(f_css.read())

    def load_file(self):
        """Display the window to select the lvm file."""
        # if self.filename_data is None:
        self.filename_data = str(QFileDialog.getOpenFileName(self, 'OpenFile', _dataDir, "Data (*.lvm)"))
        self.fileLoadedLabel.setText(os.path.basename(self.filename_data))
        print(self.filename_data)
        if _DEBUG:
            log.debug("{} LOADED".format(self.filename_data))
        self.load_data()
        if self.has_plot:
            self.clear_plots()
        self.chargeLabel.setText("?")

    def load_data(self, sep=None, column_names=None):
        """Load data using Pandas."""
        # if column_names is None:
        #     column_names = _lvm_format['column_names']
        # if sep is None:
        #     sep = _lvm_format['sep']

        if self.filename_data is not None:
            # self.data = pd.read_csv(self.filename_data, sep=sep, names=column_names)
            if self.hasHeaderCheckBox.isChecked():
                self.data = pd.read_csv(self.filename_data, sep=None)
            else:
                self.data = pd.read_csv(self.filename_data, sep=None, header=None)
            column_names_new = [str(colname) for colname in self.data.columns.values]
            self.data.columns = column_names_new
            self.xComboBox.clear()
            self.yComboBox.clear()
            for colname in column_names_new:
                self.xComboBox.addItem(QtCore.QString(colname))
                self.yComboBox.addItem(QtCore.QString(colname))

            self.consoleTextEdit.setText(self.data.head(n=10).to_html())


def get_plot(ax, X, Y, save=False, plot_name='./plots/test_plot.pdf', plot_format='pdf'):
    """Plot using matplotlib.pyplot."""
    #ax.plot(data['t'], data['I'], ls='-', color='b', lw=2)
    ax.plot(X, Y, ls='-', color='b', lw=2)

    # plt.xlim(-100, 6000)
    # ax.set_xlabel(r'$t$ (sec)')
    # ax.set_ylabel(r'$I$ (Amp)')

    # plt.yscale('log')
    # plt.legend()
    if save:
        plt.savefig(plot_name, format=plot_format)
        if _DEBUG:
            log.debug("{} Saved".format(plot_name))
            print("Plot: {} Saved".format(plot_name))



def index_of(the_list, value):
    """Return index of the array element with }or just below the value."""
    if value < min(the_list):
        return 0
    return max(np.where(the_list <= value)[0])


def get_charge(data, a=None, b=None):
    """Get the deposited charge between t=a to t=b.

    Here the definition of charge is taken as the area under the I curve.
    The Integral is simply approximated as the sum of the I * delta_t.
    """
    # Find the element in time array with value a and b
    # If a is not given the integral will include ALL the
    # deposited charge

    # time = data['t']
    # current = data['I']
    time = data['1']
    current = data['2']

    if a is None:
        a_idx = 0
    else:
        a_idx = index_of(time, a)
    if b is None:
        b_idx = len(time) - 1
    else:
        b_idx = index_of(time, b)

    delta_t = time[20] - time[19]  # Here we suppose that delta_t is constant
    charge = sum(current[a_idx:b_idx]) * delta_t
    return charge


def main():
    """Main."""
    app = QApplication(sys.argv)
    gui = Window()
    gui.run()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
