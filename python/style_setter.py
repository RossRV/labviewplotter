"""Style Setter for elements defined in PlotterQt.py"""


def set_styles(Window, theme):
    try:
        with open(theme + "mainWindow.css", "r") as f:
            Window.setStyleSheet(f.read())
        with open(theme + "plotWindow.css", "r") as f:
            Window.plotWindow.setStyleSheet(f.read())
        with open(theme + "plotBtn.css", "r") as f:
            Window.plotBtn.setStyleSheet(f.read())
        # with open(theme + "clearPlotBtn.css", "r") as f:
        #     Window.clearPlotBtn.setStyleSheet(f.read())
        with open(theme + "selectFileBtn.css", "r") as f:
            Window.selectFileBtn.setStyleSheet(f.read())
        with open(theme + "chargeBtn.css", "r") as f:
            Window.chargeBtn.setStyleSheet(f.read())
        with open(theme + "authorLabel.css", "r") as f:
            Window.authorLabel.setStyleSheet(f.read())
        with open(theme + "titleLabel.css", "r") as f:
            Window.titleLabel.setStyleSheet(f.read())
        with open(theme + "genericLabel.css", "r") as f:
            css = f.read()
            Window.chargeLabel.setStyleSheet(css)
            Window.fileLoadedLabel.setStyleSheet(css)
            Window.plotSavedLabel.setStyleSheet(css)
            Window.xLabel.setStyleSheet(css)
            Window.yLabel.setStyleSheet(css)
            Window.formatLabel.setStyleSheet(css)
            Window.consoleLabel.setStyleSheet(css)
            Window.consoleTextEdit.setStyleSheet(css)
            Window.hasHeaderCheckBox.setStyleSheet(css)
            Window.savePlotCheckBox.setStyleSheet(css)
            Window.xComboBox.setStyleSheet(css)
            Window.yComboBox.setStyleSheet(css)
            Window.formatComboBox.setStyleSheet(css)

        # with open(theme + "quitBtn.css", "r") as f:
        #    Window.quit_btn.setStyleSheet(f.read())
        print("{} style loaded!".format(theme))
    except IOError as e:
        print(e)
        print(e.strerror)
        print("{} style NOT loaded, sorry !".format(theme))
