# README

# LabViewPlotterGUI 

`LabViewPlotterGUI` is a simplistic `GUI` created to deal with
  some data created by `LabView` applications used by the `author` is 
  lab research.

## Data
  
  Some example of data `*.lvm` files are included into `./python/data/'.
  
Currently the supported files are `*.lvm` files with the format:

    TAB 't' TAB 'I'
 
<p style='color: rgb(216, 0, 32)'>
 Warning: The first TAB obeys to a typo in an old LabView app.
 </p>
 
 You can change this `format` by editing `LabViewPlotterGUI.py` 
  modifying the variable:
  
    _lvm_format = {"sep": "\t", "column_names": ['empty', 't', 'I']}

which will be used as argument to the `Pandas.read_csv` data reader.
 
## Plots

The produced plots can be optionally saved into the directory `./plots/`.

## How to Modify the GUI 

The simplest way to modify the graphical interface is by using the
 `Qt Creator` to edit the `./Qt/*` files, then translate the `ui` file
  produced into `PyQt4` using the command `pyuic4` and then connect 
  the buttons and other labels using `Python PyQt4` editing `LabViewPlotterGUI.py`.
 
 A simple `Makefile` has been included to simplify this process. There
  have been defined the targets:
    
    - make             -> This run the GUI
    - make translate   -> This translate the ui: Qt >> PyQt4
    - make clean       -> Removes rubish
    - make help        -> Shows help menu
    

# Requirements
 
 
### Qt Creator
 
 Both in `Linux` and `Mac Osx` download and install `Qt Creator` from [https://www.qt.io/download/](https://www.qt.io/download/)
 
### PyQt4 package
 
 
#### In `Linux`:
 
 
     sudo apt-get install python-qt4
     sudo apt-get isntall pyqt4-dev-tools
     
     
#### In `Mac OsX`:
 
 
     brew install pyqt

For more info about `brew` installer see [http://brew.sh/](http://brew.sh/)
 
 
## Python packages
 
 
 
 - python 2.7 (Now runs with `Python 3` too!)
 - seaborn
 - numpy
 - pandas
 - matplotlib
 - pyqt


Assuming that you have `Python` installed into your machine, the easiest way 
to install all these modules is by typing into the `Terminal`:

    pip install seaborn


as `Seaborn` has the rest of packages in its own requirements.

